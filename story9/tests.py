from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User
# Create your tests here.

c = Client()

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time

class Test_Story_9(TestCase):
    def test_login_link(self):
        response = c.get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        data = {'username':'test', 'password':'test'}

        response = c.post('/story9/login/', data=data)

        response2 = c.get('/story9/login/')
        self.assertEqual(response2.status_code, 302)

        response3 = c.get('/story9/')
        self.assertEqual(response3.status_code, 200)

    def test_link_home_not_logged_in(self):
        response = c.get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_link_logout_redirect(self):
        response = c.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

class Functional_Test(LiveServerTestCase):
    def setUp(self):
        # options = Options()
        # self.browser = webdriver.Firefox(executable_path="DRIVER/geckodriver")
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)
    
    def tearDown(self):
        self.browser.quit()

    def test_next_link(self):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        
        self.browser.get(self.live_server_url + "/story9/")
        time.sleep(1)
        
        username = self.browser.find_element_by_id('id_username')
        username.send_keys("test")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys("test")
        button = self.browser.find_element_by_class_name('button')
        button.click()

        time.sleep(1)