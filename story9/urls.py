from django.urls import path
from . import views

app_name = "story9"

urlpatterns = [
    path('login/', views.story9_login, name = 'login'),
    path('logout/', views.story9_logout, name = 'logout'),
    path('', views.story9, name= 'home'),
]