from django.shortcuts import render, redirect

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.
def story9_login(request):
    text = ""
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('story9:home')
        else :
            text = "Enter valid username and password!"
    else :
        if request.user.is_authenticated:
            return redirect('story9:home')
        else:
            form = AuthenticationForm()
    return render(request, 'story9_login.html', {'form':form, 'text':text})

@login_required(login_url='story9:login')
def story9(request):
    return render(request, 'story9.html')

def story9_logout(request):
    logout(request)
    return redirect('story9:login')