$(document).ready(() => {
    $.ajax({
        method: 'GET',
        url : 'https://www.googleapis.com/books/v1/volumes?q=random',
        success: function(response) {
            $('tbody').empty();
            for(let i=0; i < response.items.length; i++) {
                var row = document.createElement('tr');
                $(row).append('<th scope="row">' + (i+1) + '</th>');
                $(row).append('<td class="judul">' + response.items[i].volumeInfo.title + '</td>');
                try {
                    $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                }
                catch {
                    $(row).append('<td class="deskripsi">No description available</td>');
                }
                $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>')
                try {
                    $(row).append('<td class="gambar"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                }
                catch(e) {
                    $(row).append('<td class="gambar">No image available</td>')
                }
                $('tbody').append(row);
            }
        }
    })


    $('#button').click(function() {
        let key  = $('#search').val();
        $.ajax({
            method: 'GET',
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + key,
            success: function(response) {
                $('tbody').empty();
                for(let i=0; i < response.items.length; i++) {
                    var row = document.createElement('tr');
                    $(row).append('<th scope="row">' + (i+1) + '</th>');
                    $(row).append('<td class="judul">' + response.items[i].volumeInfo.title + '</td>');
                    try {
                        $(row).append('<td class="deskripsi">' + response.items[i].volumeInfo.description + '</td>');
                    }
                    catch {
                        $(row).append('<td class="deskripsi">No description available</td>');
                    }
                    $(row).append('<td class="link">' + response.items[i].volumeInfo.authors + '</td>')
                    try {
                        $(row).append('<td class="gambar"><img src="' + response.items[i].volumeInfo.imageLinks.thumbnail + '"></td>');
                    }
                    catch(e) {
                        $(row).append('<td class="gambar">No image available</td>')
                    }
                    $('tbody').append(row);
                }
            }
        })
    })
})