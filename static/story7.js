$(document).ready(function(){
    if($('.toggle').prop('checked', false)) {
        $("#container").toggleClass("");
        $("#accordion h3").toggleClass("accLight")
    } else {
        $("#container").toggleClass("dark");
    }
})

$(function() {
    $(".toggle").click(function() {
        $("#container").toggleClass("dark");
        $("#accordion h3").toggleClass("accDark");
    })
})

$(function() {
    $('#accordion > .accContent').hide();

    $('#accordion > .borderAcc').each(function() {
        $(this).click(function() {
            if($(this).next(".accContent").is(':visible')) {
                $(this).next(".accContent").slideUp();
            } else {
                $('#accordion > .accContent').slideUp();
                $(this).next(".accContent").slideDown();
            }
        })
    })
})