from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.
def story8_page(request):
    return render(request, "story8.html")