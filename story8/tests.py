from django.test import TestCase, Client
from django.urls import resolve
from . import views

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time
# Create your tests here.
c = Client()
class Test_Story8(TestCase) :
    def test_ada_link(self):
        response = c.get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_using_func(self):
        response = resolve('/story8/')
        self.assertEqual(response.func, views.story8_page)

    def test_using_template(self):
        response = c.get('/story8/')
        self.assertTemplateUsed(response, 'story8.html')

class Functional_Test(LiveServerTestCase):
    def setUp(self):
        # options = Options()
        # self.browser = webdriver.Firefox(executable_path="DRIVER/geckodriver")
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser  = webdriver.Chrome(chrome_options=chrome_options)

    
    def tearDown(self):
        self.browser.quit()

    def test_ada_search_box(self):
        self.browser.get(self.live_server_url + "/story8/")
        time.sleep(1)
        self.assertIn('id="search"', self.browser.page_source)
    
    def test_ada_button(self):
        self.browser.get(self.live_server_url + "/story8/")
        time.sleep(1)
        self.assertIn('id="button"', self.browser.page_source)

    def test_title(self):
        self.browser.get(self.live_server_url + "/story8/")
        self.assertIn("BOOK SEARCHER", self.browser.page_source)
    
